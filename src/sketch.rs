use std::f64::consts::{FRAC_PI_2, PI};

use color_eyre::eyre::eyre;
use image::{ImageBuffer, Rgba, RgbaImage};
use rand::{prelude::SmallRng, Rng};
use tiny_skia::{Color, FillRule, Paint, PathBuilder, Pixmap, Shader, Stroke, Transform};

pub struct UserParams {
    pub dest_width: u32,
    pub dest_height: u32,
    pub stroke_ratio: f64,
    pub stroke_reduction: f64,
    pub stroke_jitter: u32,
    pub stroke_inversion_threshold: f64,
    pub initial_alpha: f64,
    pub alpha_increase: f64,
    pub min_edge_count: u32,
    pub max_edge_count: u32,
}

impl Default for UserParams {
    fn default() -> Self {
        Self {
            dest_width: 2000,
            dest_height: 2000,
            stroke_ratio: 0.75,
            stroke_reduction: 0.002,
            stroke_inversion_threshold: 0.05,
            stroke_jitter: (0.1 * 2000 as f64) as u32,
            initial_alpha: 0.1,
            alpha_increase: 0.06,
            min_edge_count: 3,
            max_edge_count: 4,
        }
    }
}

pub struct Sketch {
    pub params: UserParams,
    source: RgbaImage,
    canvas: tiny_skia::Pixmap,
    source_width: u32,
    source_height: u32,
    pub stroke_size: f64,
    initial_stroke_size: f64,
}

impl Sketch {
    pub fn new(source: RgbaImage, params: UserParams) -> color_eyre::Result<Self> {
        let source_width = source.width();
        let source_height = source.height();

        let initial_stroke_size = params.stroke_ratio * params.dest_width as f64;
        let stroke_size = initial_stroke_size;

        let mut canvas =
            Pixmap::new(params.dest_width, params.dest_height).ok_or_else(|| eyre!("pixmap"))?;
        canvas.fill(Color::BLACK);

        Ok(Self {
            params,
            source,
            canvas,
            source_width,
            source_height,
            stroke_size,
            initial_stroke_size,
        })
    }

    pub fn update(&mut self, rnd_x: u32, rnd_y: u32, rng: &mut SmallRng) -> color_eyre::Result<()> {
        let pixel = self.source.get_pixel(rnd_x, rnd_y);
        let [r, g, b, _] = pixel.0;

        let mut dest_x = rnd_x as f64 * self.params.dest_width as f64 / self.source_width as f64;
        dest_x += rand_range(self.params.stroke_jitter as i32, rng);
        let mut dest_y = rnd_y as f64 * self.params.dest_height as f64 / self.source_height as f64;
        dest_y += rand_range(self.params.stroke_jitter as i32, rng);

        let edges = rng.gen_range(self.params.min_edge_count..=self.params.max_edge_count);

        let path = draw_rectangular_polygon(
            edges,
            dest_x,
            dest_y,
            self.stroke_size,
            rng.gen_range(0.0..2.0 * PI),
        )
        .ok_or_else(|| eyre!("path"))?;

        let fill_paint = Paint {
            shader: Shader::SolidColor(Color::from_rgba8(r, g, b, self.params.initial_alpha as u8)),
            anti_alias: true,
            ..Default::default()
        };
        self.canvas.fill_path(
            &path,
            &fill_paint,
            FillRule::default(),
            Transform::identity(),
            None,
        );

        let stroke_paint = if self.stroke_size
            <= self.params.stroke_inversion_threshold * self.initial_stroke_size
        {
            if (r as u16 + g as u16 + b as u16) / 3 < 128 {
                Paint {
                    shader: Shader::SolidColor(Color::from_rgba8(
                        255,
                        255,
                        255,
                        self.params.initial_alpha as u8,
                    )),
                    anti_alias: true,
                    ..Default::default()
                }
            } else {
                Paint {
                    shader: Shader::SolidColor(Color::from_rgba8(
                        0,
                        0,
                        0,
                        self.params.initial_alpha as u8,
                    )),
                    anti_alias: true,
                    ..Default::default()
                }
            }
        } else {
            fill_paint
        };

        self.canvas.stroke_path(
            &path,
            &stroke_paint,
            &Stroke::default(),
            Transform::identity(),
            None,
        );

        self.stroke_size -= self.params.stroke_reduction * self.stroke_size;
        self.params.initial_alpha += self.params.alpha_increase;
        Ok(())
    }

    pub fn output(&self) -> color_eyre::Result<ImageBuffer<Rgba<u8>, &[u8]>> {
        let image = ImageBuffer::from_raw(
            self.params.dest_width,
            self.params.dest_height,
            self.canvas.data(),
        )
        .ok_or_else(|| eyre!("not a valid image"))?;
        Ok(image)
    }
}

fn rand_range(stroke_jitter: i32, rng: &mut SmallRng) -> f64 {
    rng.gen_range(-stroke_jitter..=stroke_jitter) as f64
}

fn draw_rectangular_polygon(
    edges: u32,
    x: f64,
    y: f64,
    r: f64,
    rotation: f64,
) -> Option<tiny_skia::Path> {
    // dbg!(edges, x, y, r, rotation);
    let angle = 2.0 * PI / edges as f64;
    let rotation = {
        let mut r = rotation - FRAC_PI_2;

        if edges % 2 == 0 {
            r += angle / 2.0;
        }
        r
    };

    let mut path = PathBuilder::new();

    let a = rotation + angle * 0.0 as f64;
    let start_x = (x + r * a.cos()) as f32;
    let start_y = (y + r * a.sin()) as f32;
    path.move_to(start_x, start_y);

    for i in 1..edges {
        let a = rotation + angle * i as f64;
        let x = (x + r * a.cos()) as f32;
        let y = (y + r * a.sin()) as f32;
        path.line_to(x, y);
    }

    // path.line_to(start_x, start_y);

    path.close();
    path.finish()
}
