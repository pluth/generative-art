use std::{io::Read, path::Path};

use image::{GenericImageView, ImageBuffer, Rgba, RgbaImage};
use rand::{
    distributions::WeightedIndex,
    prelude::{Distribution, SmallRng},
    thread_rng, Rng, SeedableRng,
};
use sketch::{Sketch, UserParams};
use tracing_subscriber::fmt::format::FmtSpan;

mod sketch;

fn main() -> color_eyre::Result<()> {
    tracing_subscriber::fmt()
        .with_span_events(FmtSpan::CLOSE)
        .init();

    // let args = std::env::args().nth(1);

    // let image = if let Some(path) = args {
    //     load_image(path)?
    // } else {
    //     let span = tracing::info_span!("download").entered();
    //     let image = load_random_image(2000, 2000)?;
    //     drop(span);
    //     {
    //         let _span = tracing::info_span!("save original").entered();
    //         image.save("original.jpeg")?;
    //     }
    //     image
    // };

    let image = load_image("cia-orig.jpg")?;
    let image_filter = {
        let buffer = std::fs::read("cia-filter.jpg")?;

        let image = image::load_from_memory(&buffer)?;
        let image = image.into_luma8();
        image
    };
    dbg!(image.dimensions());
    dbg!(image_filter.dimensions());
    let pixels = image_filter.width() * image_filter.height();
    dbg!(pixels, image_filter.as_raw().len());

    tracing::info!(dim=?image.dimensions(), "image");
    let dest_width = image.width();
    let dest_height = image.height();

    let mut rng = SmallRng::from_rng(&mut thread_rng())?;

    let dist = WeightedIndex::new(image_filter.as_raw())?;
    let num_pixels = image.width() * image.height();

    let total_cycle_count = 5000;
    let mut sketch = Sketch::new(
        image,
        UserParams {
            stroke_ratio: 0.75,
            dest_width,
            dest_height,
            initial_alpha: 0.1,
            stroke_reduction: 0.002,
            alpha_increase: 0.06,
            stroke_inversion_threshold: 0.05,
            stroke_jitter: (0.1 * dest_width as f64) as u32,
            min_edge_count: 3,
            max_edge_count: 8,
        },
    )?;

    {
        let _span = tracing::info_span!("render").entered();
        // for _i in 0..total_cycle_count {
        //     let pixel = rng.gen_range(0..num_pixels);
        //     let x = pixel as u32 % dest_width;
        //     let y = pixel as u32 / dest_width;
        //     sketch.update(x, y, &mut rng)?;
        // }
        dbg!(sketch.stroke_size, sketch.params.initial_alpha);
        sketch.params.alpha_increase = 0.0;
        sketch.params.stroke_reduction = 0.0;
        sketch.stroke_size = 10.0;
        sketch.params.initial_alpha = 255.0;
        for _i in 0..10000 {
            let pixel = dist.sample(&mut rng);
            let x = pixel as u32 % dest_width;
            let y = pixel as u32 / dest_width;
            sketch.update(x, y, &mut rng)?;
        }
    }

    {
        let _span = tracing::info_span!("save render").entered();
        save_output(sketch.output()?, "output.png")?;
    }

    Ok(())
}

pub fn load_image(path: impl AsRef<Path>) -> color_eyre::Result<RgbaImage> {
    let buffer = std::fs::read(path)?;

    let image = image::load_from_memory(&buffer)?;

    Ok(image.into_rgba8())
}

pub fn load_random_image(width: u32, height: u32) -> color_eyre::Result<RgbaImage> {
    let url = format!("https://source.unsplash.com/random/{}x{}", width, height);
    let resp = ureq::get(&url).call()?;

    assert!(resp.has("Content-Length"));
    let len = resp
        .header("Content-Length")
        .and_then(|s| s.parse::<usize>().ok())
        .unwrap();

    let mut bytes: Vec<u8> = Vec::with_capacity(len);
    resp.into_reader()
        .take(10_000_000)
        .read_to_end(&mut bytes)?;

    let image = image::load_from_memory(&bytes)?;
    Ok(image.into_rgba8())
}

fn save_output(
    image: ImageBuffer<Rgba<u8>, &[u8]>,
    path: impl AsRef<Path>,
) -> color_eyre::Result<()> {
    image.save(path)?;
    Ok(())
}
